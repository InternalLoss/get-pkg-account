# PKG Entitlement CLI

This tool allows you to retrieve PKG links for titles you own on your PSN account.

Example usage: `go run main.go list --all-titles --ssostring "REDACTED"`

```
Usage:
  get-pkg-account list [flags]

Flags:
      --all-titles           list titles including ps4 and ps5(there won't be pkg links however), default to false, only for csv
  -h, --help                 help for list
      --output-name string   split file output (<name>-0.json, etc) (default "psdle")
      --psdle                PSDLE output format for NPS, default is csv
      --purchase-date        add purchased date to the export, default to false, only for csv
      --split                Split output, use with --psdle
      --split-size int       Split size (default 300)
      --ssotoken string      SSO Token from https://ca.account.sony.com/api/v1/ssocookie
```
